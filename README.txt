+ To run all the simulations run ./src/main.m;

+ To run the simulations from exercise 1, run ./src/main_new.m

+ To run the simulations from exercice 2, run ./src/main_ofdm.m

+ To run the simulations from exercice 3, after having already ran the
  simulations from the previous exercises, run ./src/main_compare.m and
  then run main_distrib.m.
