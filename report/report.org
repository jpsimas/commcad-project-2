#+TITLE: Project 2 Report
#+SUBTITLE: 01NVTOQ - Computer aided design of communication systems
#+AUTHOR: João Pedro de Omena Simas
#+DATE:
#+OPTIONS: toc:nil

\pagebreak

\tableofcontents

\pagebreak

* Part 1
  For this part the QPSK transmitter, a receiver based on an
  DFE/FFE equalizer and also a function that generates a realization
  of a multi path Rayleigh fading were written in MATLAB/OCTAVE. Those
  can be found in the code listing on appendix 1. A diagram of the
  QPSK receiver transmitter can be seen below:

  #+NAME: genblk
  #+BEGIN_SRC ditaa :file blockdgram.png :noexport
                                 +--------+
				 |Gaussian|
                                 |Noise   |---+
                                 |Source  |   |
				 +--------+   v
  +--------+  +----------+  +-------------+  +-+  +----------+
  | Symbol |->|Up Sampler|->|RRC          |->|+|->|Rayleigh  |
  | Source |  +----------+  |Pulse Shaping|  +-+  |Multi Path|
  +--------+                +-------------+       |Channel   |
                                                  +----------+
  +--------+  +---------+  +---------+   +--------+     |
  |BER     |<-|Detection|<-|DFE/LMS  | <-|Down    |-----+
  |Counting|  +---------+  |Equalizer|   |Sampler |
  +--------+               +---------+   |to 2 sps|
                                         +--------+
  #+END_SRC

  #+CAPTION: Block diagram of the QPSK system simulation.
  #+RESULTS: genblk
  [[file:blockdgram.png]]

  All the simulations were run with a up sampling factor of sps = 4 for
  the channel simulation and a bit rate of \(18 \times \frac{5}{4}\)
  Mbit/s, so we get an effective bit rate of 18 Mbit/s using 20% of
  the frame for training symbols. Also all the simulations were
  carried out transmitting two symbols and ignoring the errors in the
  first symbol.
** Item A
   Using both the FFE (n_{FFE} = 32) and DFE equalizers (n_{FFE} = 32,
   n_{DFE} = 5) and an AWGN channel to run a simulation with \(\mu =
   10^{-3}\) and the frame length set to 10000 symbols we get the
   following BER curves: 

   \pagebreak
   #+CAPTION: BER results for the QPSK system with the FFE equalizer.
   [[../src/1_a_ffe_ber.png]]

   #+CAPTION: BER results for the QPSK system with the DFE equalizer.
   [[../src/1_a_dfe_ber.png]]

   \pagebreak

   It can be seen that the results match the ones of the ideal QPSK
   transmitter/receiver on an AWGN channel.

   Also, the target SNRs, i.e. the SNR that gives the target BER of
   10^{-3} were:
   \[SNR_{target, FFE} \approx 10.62 dB\]
   \[SNR_{target, DFE} \approx 10.25 dB\]

** Item B
   The \mu parameter of the equalizer and frame length were optimized
   by taking 10 realizations of the specified Rayleigh channel and
   running simulation of the system with different parameter
   combinations in the specified ranges and then taking the parameters
   that minimized the mean BER value. The results of said simulations
   can be seen below in the form of contour plots:

   \pagebreak
   #+CAPTION: Contour plot of BER at SNR = \(SNR_{target, FFE} \times 10^{1.1}\) for the QPSK system with the FFE equalizer varying \mu and the frame length.
   [[../src/1_b_countour_ffe.png]]
   #+CAPTION: Contour plot of BER at SNR = \(SNR_{target, DFE} \times 10^{1.1}\) for the QPSK system with the DFE equalizer varying \mu and the frame length.
   [[../src/1_b_countour_dfe.png]]
   \pagebreak

   Note that those simulations were run with SNRs 1dB higher than the
   reference SNRs obtained in the previous item.

   The optimum parameters were found to be:
   \[\mu_{FFE, opt} = \mu_{DFE, opt} \approx 2.51 \times 10^{-3}\]
   \[frameLength_{FFE, opt} = frameLength_{DFE, opt} \approx 10000\]

   Finally, the simulations ran before where run again to find the
   reference SNRs with those new parameters in the back-to-back
   condition. The new curves obtained were:

   #+CAPTION: BER results for the QPSK system with the FFE equalizer with optimized \mu and frame length.
   [[../src/1_a_opt_ffe_ber.png]]
   #+CAPTION: BER results for the QPSK system with the DFE equalizer with optimized \mu and frame length.
   [[../src/1_a_opt_dfe_ber.png]]   
   
   \pagebreak

   Together with the new reference SNRs
   \[SNR_{target, FFE} \approx 10.69 dB\]
   \[SNR_{target, DFE} \approx 10.55 dB\]
    
* Part 2
  To run the simulations of the OFDM system, the provided receiver and
  transmitter functions were used. Also an up sampling factor of sps =
  10 was used, because it was empirically determined to be the one to
  work best with the provided receiver/transmitter.

  By running a simulation with an AWGN channel, the following BER
  curve was calculated: 
  [[../src/2_ofdm_ber.png]]
  It can be seen that the results are very close to the ones from the
  ideal QPSK systems in an AWGN channel.
  Also the target SNR, i.e. the SNR that gives the specified
  target BER of 10^{-3} was calculated to be:
  \[SNR_{target, OFDM} \approx 10.45 dB\]

* Part 3
  For the statistical analysis of the receivers with the Rayleigh,
  simulations were run for 100 different realizations of the channel
  with SNRs equal to SNRs 1dB higher to their respective reference
  SNRs.
  Then the empirical cumulative distribution functions were estimated
  and plotted:
  
  #+CAPTION: Cumulative distribution function estimated by running simulations with multple realizations of the rayleigh channel using the three previously mentioned receivers with the optimized parameters and SNRs 1dB higher than their respective target SNRs.
  [[../src/3_cdf.png]]
  And from those the outage probabilities were estimated by
  interpolating the estimated curves:

  \[P_{outage, FFE} \approx P_{outage, DFE} \approx 5 \times 10^{-2}\]
  \[P_{outage, OFDM} \approx 4 \times 10^{-2}\]

  It is worth noting that the curves differ considerable from those
  shown as the expected result doing class, however, those curves seem
  to have been plotted with much lower \mu values for the LMS
  equalizers. To illustrate this, it is shown below a cdf estimate
  using \mu = 10^{-4}:

  #+CAPTION: Cumulative distribution function estimated by running simulations with multple realizations of the rayleigh channel using the three previously mentioned receivers with the optimized frame length and \mu = 10^{-4} and SNRs 1dB higher than their respective target SNRs.
  [[../src/3_cdf_mu1e-4.png]]
  
  \pagebreak

  This matches those results more closely, as decreasing the \mu makes
  the system more sensitive to variations of the channel, as the system
  will take to long to converge in some of the cases and, as
  previously mentioned the simulations were run with two symbols
  ignoring the errors in the first one, thus adding the constraint
  that the convergence of the filter should happen in the time of one
  symbol, increasing the lower bound of \mu values.

  Also, it should be pointed out that 100 realizations were used
  instead of 1000, because that last number would take approximately
  one hour to run, making it unfeasible to run that many realizations.

  \pagebreak

* Appendix 1 - Code Listing 
  The full listing of the octave code used for the simulations can be
  found in the post ceding pages. It can be also found at the git
  repository https://gitlab.com/jpsimas/commcad-project-2.
** main\textunderscore new.m
   This files runs the simulations relative to part 1, using the
   qpsk_sim function.
   #+INCLUDE: "../src/main_new.m" src octave
** qpsk\textunderscore sim.m
   This function runs a simulation of the QPSK system with the DFE-LMS
   equalizer and returns the ber and the error of the LMS filter in
   the equalizer.
   #+INCLUDE: "../src/qpsk_sim.m" src octave
** generateRayleighChannel.m
   This function generates a realization of the impulse response of
   the Rayleigh multi path channel with the given path delays and mean
   path gains.
   #+INCLUDE: "../src/generateRayleighChannel.m" src octave
** main\textunderscore ofdm.m
   This file runs the simulations relative to part 2, using the
   ofdm_sim function.
   #+INCLUDE: "../src/main_ofdm.m" src octave
** ofdm\textunderscore sim.m
   This function runs a simulation of the OFDM system using the
   provided functions.
   #+INCLUDE: "../src/ofdm_sim.m" src octave
** main\textunderscore compare.m
   This function runs the simulations relative to part three, by
   generating a set number of realizations of the Rayleigh channel and
   simulating each of the systems (FFE, DFE, OFDM) with it.
   #+INCLUDE: "../src/main_compare.m" src octave
** calc\textunderscore cdf.m
   This function given a vector of samples of a real random variable
   and a range of values, computes the empirical cumulative
   distribution function of said variable.
   #+INCLUDE: "../src/calc_cdf.m" src octave
** main\textunderscore distrib.m
   This function takes the results generated by main_compare, plots
   the empirical CDFs and estimates the outage probabilities.
   #+INCLUDE: "../src/main_distrib.m" src octave

* Generate Zip Script :noexport:
# pack all needed files into a zip archive
#+BEGIN_SRC shell :noexport :results silent
cd ..
zip -FS Project2.zip \
./report/report.pdf \
./README.txt \
./src/main.m \
./src/main_new.m \
./src/qpsk_sim.m \
./src/filter_fir.m \
./src/generateRayleighChannel.m \
./src/main_ofdm.m \
./src/ofdm_sim.m \
./src/main_compare.m \
./src/main_distrib.m \
./src/calc_cdf.m \
./src/calculateOFDMBERQPSK.m \
./src/generateOFDMSignalQPSK.m \
./src/getOFDMPreambleAndPilot.m \
./src/OFDMReceiverQPSK.m \
./src/OFDMTransmitterQPSK.m \
./src/receiveOFDMSignalQPSK.m
cd -
#+END_SRC
