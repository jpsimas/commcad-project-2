classdef (StrictDefaults)OFDMReceiverQPSK < matlab.System
% OFDMReceiver Recover payload message from the time domain OFDM input
% signal based on the 802.11a standard.

properties (Nontunable)
    %SampleRate Sample rate
    SampleRate  = 20e6
    %FrameLength Frame length
    FrameLength = 1280
end

properties (Logical, Nontunable)    
    %DisplayMessage Display decoded message
    DisplayMessage = true
    %ShowScopes Show scopes
    ShowScopes = false
end

properties(Nontunable, Access = private)
    pNumOFDMSymbols         % Number of OFDM symbols per frame
    pNumBitsPerDisplay      % Number of bits converted to letters for display
    pPilotIndexInPreamble   % Pilot subcarrier indices in preamble symbols
    pDataIndexInPreamble    % Data subcarrier indices in preamble symbols
    pPreamble               % Preamble for each frame
    pPilots                 % Pilots for each frame
    pBufferLength           % Moving buffer size at receiver
    pSlideDistance          % Incremental distance window
    pQPSKDemod              % QPSK Demodulator System object
    pPreambleOFDMDemod      % OFDM Demodulator System object for preamble
    pDataOFDMDemod          % OFDM Demodulator System object for data
    pScopes                 % OFDM Scopes System object
end

properties(Access = private) 
    pFreqOffsetEstBuffer    % Buffer for estimating frequency offset
    pNumFrameDetected       % Number of detected frames
end 

properties(Constant, Access = private) 
    % OFDM Demodulator configuration
    FFTLength             = 64 
    NumGuardBandCarriers  = [6; 5]
    NumDataCarriers       = 48
    CyclicPrefixLength    = 16
    PilotCarrierIndices   = [12;26;40;54]
    NumOFDMSymInPreamble  = 5
    NumBitsPerCharacter   = 1
    BufferToFrameLenRatio = 1.5
    FrameToSliderLenRatio = 4
    
    % Long Preamble
    LongPreamble = complex(... 
        [ 1,  1, -1, -1,  1,  1, -1,  1, -1,  1,  1,  1,...
          1,  1,  1, -1, -1,  1,  1, -1,  1, -1,  1,  1,  1,  1, 0,...
          1, -1, -1,  1,  1, -1,  1, -1,  1, -1, -1, -1, -1, -1,...
          1,  1, -1, -1,  1, -1,  1, -1,  1,  1,  1,  1].', 0)

    % A few adjustable algorithms parameters:
    %   1). Number of frequency estimates to be averaged over for frequency corrections
    NumFreqToAverage = 15 
    %   2). Peak threshold after normalization for preamble detection 
    PeakThreshold = 0.6 
    %   3). Mininum number of peaks for positive match in frame detection
    MinNumPeaksForMatch = 5
end

methods
  function obj = OFDMReceiverQPSK(varargin)
    setProperties(obj, nargin, varargin{:});
  end
  
  function set.SampleRate(obj, Rs)
    propName = 'SampleRate';
    validateattributes(Rs, {'double'}, ...
        {'real','scalar','positive','finite'}, ...
        [class(obj) '.' propName], propName); 
    
	obj.SampleRate = Rs;
  end
  
  function set.FrameLength(obj, frameLen)
    propName = 'FrameLength';
    validateattributes(frameLen, {'double'}, ... 
        {'real','scalar','positive','integer','finite'}, ... 
        [class(obj) '.' propName], propName); 
    
    obj.FrameLength = frameLen;
  end  
end

methods(Access = protected)
  function validateInputsImpl(obj, x)
    % Validate data input
    validateattributes(x, {'double'}, {'column','nonempty','finite'}, ...
        [class(obj) '.' 'Input'], 'the signal input');      
  end
  
  function setupImpl(obj)
    coder.extrinsic('setdiff', 'getOFDMPreambleAndPilot');
    
    % Constant calculation
    obj.pNumOFDMSymbols = (obj.FrameLength - obj.NumOFDMSymInPreamble * obj.FFTLength) / ...
        (obj.FFTLength + obj.CyclicPrefixLength);
    obj.pNumBitsPerDisplay = floor(obj.pNumOFDMSymbols*...
        obj.NumDataCarriers/obj.NumBitsPerCharacter*2)*obj.NumBitsPerCharacter;
    obj.pBufferLength  = ceil(obj.FrameLength*obj.BufferToFrameLenRatio);
    obj.pSlideDistance = ceil(obj.FrameLength/obj.FrameToSliderLenRatio);  

    % Calculate locations of pilots without guardbands in preambles
    obj.pPilotIndexInPreamble = obj.PilotCarrierIndices - obj.NumGuardBandCarriers(1);
    
    % Calculate locations of data without guardbands in preambles
    obj.pDataIndexInPreamble  = coder.const(double(setdiff((1:length(obj.LongPreamble))', ...
        sort([obj.PilotCarrierIndices; obj.FFTLength/2+1] - obj.NumGuardBandCarriers(1)))));    
    
    % Get preamble for each frame
    obj.pPreamble = coder.const(double(getOFDMPreambleAndPilot('Preamble', obj.FFTLength, obj.NumGuardBandCarriers)));
    
    % Get pilot for each frame
    obj.pPilots = coder.const(double(getOFDMPreambleAndPilot('Pilot', obj.pNumOFDMSymbols)));
    
    % QPSK demodulator
    obj.pQPSKDemod = comm.QPSKDemodulator('BitOutput',true);

    % OFDM demodulator for preamble
    obj.pPreambleOFDMDemod = comm.OFDMDemodulator( ...
        'FFTLength' ,           obj.FFTLength, ...
        'NumGuardBandCarriers', obj.NumGuardBandCarriers, ...
        'CyclicPrefixLength',   0, ...
        'NumSymbols',           2);         
    
    % OFDM demodulator for data
    obj.pDataOFDMDemod = comm.OFDMDemodulator( ...
        'FFTLength' ,           obj.FFTLength, ...
        'NumGuardBandCarriers', obj.NumGuardBandCarriers, ...
        'RemoveDCCarrier',      true, ...
        'PilotOutputPort',      true, ...
        'PilotCarrierIndices',  obj.PilotCarrierIndices, ...
        'CyclicPrefixLength',   obj.CyclicPrefixLength, ...
        'NumSymbols',           obj.pNumOFDMSymbols);
  end
  
  function resetImpl(obj)
    obj.pFreqOffsetEstBuffer = zeros(1, obj.NumFreqToAverage);
    obj.pNumFrameDetected = 0;    
    
    reset(obj.pQPSKDemod);
    reset(obj.pPreambleOFDMDemod);
    reset(obj.pDataOFDMDemod);    
  end

  function [y, numFramesDetected] = stepImpl(obj, x)
    coder.extrinsic('num2str','processOFDMScopes');
    
    frameLen = obj.FrameLength;
    bufferLen = min(obj.pBufferLength, length(x));
    detFrmAtBeginning  = obj.pNumFrameDetected;
    
    % Start index for the last moving buffer in this input
    lastBufferStartIdx = max(1, length(x) - bufferLen + 1); 
    
    % Initialization
    y = zeros(1, 0);  
    bufferStartIdx = 1;    
    if obj.ShowScopes
        processOFDMScopes(obj.SampleRate, obj.FrameLength, 'reset');
    end

    % Moving buffer for frame detection and message recovery        
    while bufferStartIdx <= lastBufferStartIdx
        % Current buffer
        buffer = x(bufferStartIdx + (0:bufferLen-1));
        
        % Find preamble in buffer: Return -1 if finding nothing
        preambleStartLocation = locatePreamble(obj, buffer);

        % Check if this buffer holds a full frame
        frameDetected = (preambleStartLocation ~= -1 ) && ...
            ((preambleStartLocation + obj.FrameLength) <= bufferLen);

        % Recover message from the detected frame
        if frameDetected
            obj.pNumFrameDetected = obj.pNumFrameDetected + 1;

            % Extract single frame from buffer
            oneFrameToProcess = buffer(preambleStartLocation + (1:frameLen));

            % Correct frequency offset
            [oneFrameToProcess, estFreqOffset] = coarseFreqCorrection(obj, oneFrameToProcess);

            % Apply equalizers
            [postEqData, preEqData, eqGains] = frameEqualization(obj, oneFrameToProcess);
            
            % QPSK demodulation
            msgInBits = obj.pQPSKDemod(postEqData(:)); 
            
            % Save bits to output
            y = [y, msgInBits(1:obj.pNumBitsPerDisplay)']; %#ok<AGROW>
            
            % Convert bits to message and display it as specified
            if obj.DisplayMessage 
                msgInBits = reshape(msgInBits(1:obj.pNumBitsPerDisplay), ...
                                    [7, obj.pNumBitsPerDisplay/7])';
                disp(char(bin2dec(num2str(msgInBits)))'); 
            end

            if obj.ShowScopes % Visualization
                processOFDMScopes(obj.SampleRate, obj.FrameLength, 'step', ...
                    bufferStartIdx+preambleStartLocation, estFreqOffset, ...
                    oneFrameToProcess, eqGains, preEqData, postEqData)
            end
                        
            if bufferStartIdx < lastBufferStartIdx % Jump a frame, about 4 windows
                bufferStartIdx = min(bufferStartIdx + frameLen, lastBufferStartIdx);
            else % Window sliding
                bufferStartIdx = bufferStartIdx + obj.pSlideDistance; 
            end            
        else % Window sliding
            bufferStartIdx = bufferStartIdx + obj.pSlideDistance; 
        end                        
    end
    
    numFramesDetected = obj.pNumFrameDetected - detFrmAtBeginning;
  end
  
  function releaseImpl(obj)
    release(obj.pQPSKDemod);
    release(obj.pPreambleOFDMDemod);
    release(obj.pDataOFDMDemod);
  end
  
  function s = saveObjectImpl(obj)
    s = saveObjectImpl@matlab.System(obj);
    if isLocked(obj)
        s.pQPSKDemod            = matlab.System.saveObject(obj.pQPSKDemod);
        s.pPreambleOFDMDemod    = matlab.System.saveObject(obj.pPreambleOFDMDemod);        
        s.pDataOFDMDemod        = matlab.System.saveObject(obj.pDataOFDMDemod);
        s.pScopes               = matlab.System.saveObject(obj.pScopes);        
        s.pNumOFDMSymbols       = obj.pNumOFDMSymbols;
        s.pNumBitsPerDisplay    = obj.pNumBitsPerDisplay;
        s.pPilotIndexInPreamble = obj.pPilotIndexInPreamble;
        s.pDataIndexInPreamble  = obj.pDataIndexInPreamble;
        s.pPreamble             = obj.pPreamble;
        s.pPilots               = obj.pPilots;
        s.pBufferLength         = obj.pBufferLength;
        s.pSlideDistance        = obj.pSlideDistance;
        s.pFreqOffsetEstBuffer  = obj.pFreqOffsetEstBuffer;
        s.pNumFrameDetected     = obj.pNumFrameDetected;
    end      
  end
  
  function loadObjectImpl(obj, s, wasLocked)
    if wasLocked
        obj.pQPSKDemod            = matlab.System.loadObject(s.pQPSKDemod);
        obj.pPreambleOFDMDemod    = matlab.System.loadObject(s.pPreambleOFDMDemod);
        obj.pDataOFDMDemod        = matlab.System.loadObject(s.pDataOFDMDemod);
        obj.pScopes               = matlab.System.saveObject(s.pScopes);
        obj.pNumOFDMSymbols       = s.pNumOFDMSymbols;
        obj.pNumBitsPerDisplay    = s.pNumBitsPerDisplay;
        obj.pPilotIndexInPreamble = s.pPilotIndexInPreamble;
        obj.pDataIndexInPreamble  = s.pDataIndexInPreamble;
        obj.pPreamble             = s.pPreamble;
        obj.pPilots               = s.pPilots;
        obj.pBufferLength         = s.pBufferLength;
        obj.pSlideDistance        = s.pSlideDistance;
        obj.pFreqOffsetEstBuffer  = s.pFreqOffsetEstBuffer;
        obj.pNumFrameDetected     = s.pNumFrameDetected;
    end
    loadObjectImpl@matlab.System(obj, s);
  end
  
  function numOut = getNumOutputsImpl(~)
    numOut = 2;
  end
  
  function flag = isInputSizeLockedImpl(~,~)
    flag = false;
  end
  
  function flag = isInputComplexityLockedImpl(~,~)
    flag = true;
  end
  
  function flag = isOutputComplexityLockedImpl(~,~)
    flag = true;
  end
end

methods (Access = private)  
  function preambleStartLocation = locatePreamble(obj, x)
    % Locate the starting point of the preamble using cross correlation.
    
    L = obj.FFTLength;
    K = obj.FFTLength/4; 
    known = obj.pPreamble(1:K);
    windowLength = ceil(0.5*obj.FrameLength + length(obj.pPreamble));

    % Cross correlate
    rWin = x(1: windowLength-L+K-1);
    Phat = xcorr(rWin, conj(known));
    Rhat = xcorr(abs(rWin).^2, ones(K,1)); % Moving average

    % Remove leading and tail zeros overlaps
    PhatShort = Phat(ceil(length(Phat)/2):end-K/2+1);
    RhatShort = Rhat(ceil(length(Rhat)/2):end-K/2+1);

    % Calculate timing metric
    M = abs(PhatShort).^2 ./ RhatShort.^2;

    % Determine start of short preamble. First find peak locations
    MLocations = find(M > (max(M)*obj.PeakThreshold));

    % Correct estimate to the start of preamble, not its center
    MLocations = MLocations - (K/2+1);

    % Determine correct peaks
    peaks = zeros(size(MLocations));
    desiredPeakLocations = (K:K:2*L)';
    for i = 1:length(MLocations)
        MLocationGuesses = MLocations(i) + desiredPeakLocations;
        peaks(i) = length(intersect(MLocations(i:end), MLocationGuesses));
    end

    % Have at least obj.pNumRequiredPeaks peaks for positive match    
    peaks(peaks < obj.MinNumPeaksForMatch) = 0;

    % Pick earliest peak in time
    [numPeaks, frontPeakLocation] = max(peaks);
    if ~isempty(peaks) && (numPeaks > 0)
        preambleStartLocation = MLocations(frontPeakLocation);
    else % No desirable location found
        preambleStartLocation = -1; 
    end
  end

  function [y, estFreqOffset] = coarseFreqCorrection(obj, x)
    % Frequency correction based on the Schmidl and Cox method, utilizing
    % halves of the short preamble from the 802.11a standard.
    
    Ts = 1/obj.SampleRate;
    halfFFTLen = obj.FFTLength/2;
    freqOffsetBufferLen = obj.NumFreqToAverage;
    
    % Cross-correlate preamble and determine phase angle
    phi = angle(sum(conj(x(1:halfFFTLen)) .* x(halfFFTLen+(1:halfFFTLen)))); 
                     
    % Update frequency offset buffer
    bufferIdx = mod(obj.pNumFrameDetected - 1, freqOffsetBufferLen) + 1;
    obj.pFreqOffsetEstBuffer(bufferIdx) = phi/(2 * pi * halfFFTLen * Ts);
    
    % Estimated frequency offset     
    estFreqOffset = mean(obj.pFreqOffsetEstBuffer(1:min(freqOffsetBufferLen, obj.pNumFrameDetected)));
 
    % Apply frequency correction
    y=x;
    %y = exp(1i * estFreqOffset * (0 : (length(x)-1))' * Ts) .* x;
  end
  
  function [postEqData, demodData, eqGains] = frameEqualization(obj, x)
    % Equalize the OFDM frame through the use of both the long preamble
    % from the 802.11a standard and four pilot tones in the data frames.
    % The gains from the pilots are interpolated across frequency and
    % applied to all data subcarriers.
    
    % Use long preamble frame to estimate channel in frequency domain
    FFTLen = obj.FFTLength;

    % Demodulate received long preamble
    recLongPreamble = x(3*FFTLen + (1:2*FFTLen));
    decLongPreamble = obj.pPreambleOFDMDemod(recLongPreamble);

    % Get preamble equalizer gains
    preambleNorm = decLongPreamble ./ [obj.LongPreamble, obj.LongPreamble];
    preambleEqGains = conj(mean(preambleNorm, 2)) ./ mean(abs(preambleNorm).^2, 2);    
    
    % Separate data from preambles and demodulate them
    recData = x(5*FFTLen+1:end);
    [demodData, demodPilots] = obj.pDataOFDMDemod(recData);
        
    % Apply preamble equalizer gains to data and pilots
    preambleEqGainsOnPilots = preambleEqGains(obj.pPilotIndexInPreamble);
    preambleEqGainsOnData   = preambleEqGains(obj.pDataIndexInPreamble);
    %preambleEqGainsOnData=ones(size(preambleEqGainsOnData,1),size(preambleEqGainsOnData,2));
    postEqPilots = repmat(preambleEqGainsOnPilots, [1, obj.pNumOFDMSymbols]) .* demodPilots;
    postEqData   = repmat(preambleEqGainsOnData,   [1, obj.pNumOFDMSymbols]) .* demodData; 
    
    % Get pilot equalizer gains
    pilotNorm = postEqPilots ./ obj.pPilots;
    pilotEqGains = conj(pilotNorm) ./ (abs(pilotNorm).^2);
    
    % Interpolate to data subcarrier size and apply pilot equalizer
    pilotEqGainsOnData = resample(pilotEqGains, obj.NumDataCarriers/size(obj.pPilots, 1), 1);
    pilotEqGainsOnData=ones(size(pilotEqGainsOnData,1),size(pilotEqGainsOnData,2));
    postEqData = pilotEqGainsOnData .* postEqData;
    eqGains = [preambleEqGainsOnData, pilotEqGainsOnData];
  end  
end

end

% [EOF]