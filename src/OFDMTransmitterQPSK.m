classdef (StrictDefaults)OFDMTransmitterQPSK < matlab.System
% OFDMTransmitter Generate OFDM signal repeatedly for a payload message
% based on the 802.11a standard.

properties (Nontunable)
    %SampleRate Sample rate    
    SampleRate = 20e6
    %PayloadMessage Payload message
    PayloadMessage = 'Live long and prosper, from the Communications Toolbox Team at MathWorks!'
    %NumFrames Number of frames to transmit
    NumFrames = 1
end

properties(Nontunable, SetAccess = private, Dependent)
    %FrameLength Frame length
    FrameLength
end

properties(Nontunable, Access = private)
    pPayloadBits        % Number of bits from payload message
    pNumPadBits         % Number of random bits padded in each frame
    pNumOFDMSymbols     % Number of OFDM symbols per frame
    pPreamble           % Preamble for each frame
    pPilots             % Pilots for each frame
    pQPSKMod            % QPSK Modulator System object
    pDataOFDMMod        % OFDM Modulator System object 
end

properties(Constant, Access = private) % OFDM Modulator configuration
    FFTLength            = 64
    NumGuardBandCarriers = [6; 5]
    NumDataCarriers      = 48
    CyclicPrefixLength   = 16
    PilotCarrierIndices  = [12;26;40;54]
    NumOFDMSymInPreamble = 5
    NumBitsPerCharacter  = 1
end

methods        
  function obj = OFDMTransmitterQPSK(varargin)
    setProperties(obj, nargin, varargin{:});
  end
  
  function set.SampleRate(obj, Rs)
    propName = 'SampleRate';
    validateattributes(Rs, {'double'}, ...
        {'real','scalar','positive','finite'}, ...
        [class(obj) '.' propName], propName); 
    
	obj.SampleRate = Rs;
  end
  
  function set.PayloadMessage(obj, msg)
    propName = 'PayloadMessage';
    validateattributes(msg, {'double',}, {'column'}, ... 
        [class(obj) '.' propName], propName); 
    
    obj.PayloadMessage = msg;
  end
    
  function set.NumFrames(obj, numFrm)
    propName = 'NumFrames';
    validateattributes(numFrm, {'double'}, ... 
        {'real','scalar','positive','integer','finite'}, ... 
        [class(obj) '.' propName], propName); 
    
    obj.NumFrames = numFrm;
  end
  
  function frameLen = get.FrameLength(obj)
    frameLen = ceil(length(obj.PayloadMessage)*obj.NumBitsPerCharacter/obj.NumDataCarriers/2) * ...
               (obj.FFTLength+obj.CyclicPrefixLength) + obj.NumOFDMSymInPreamble*obj.FFTLength;
  end
end

methods(Access = protected)
  function setupImpl(obj)
    coder.extrinsic('dec2bin','getOFDMPreambleAndPilot');
    
    % Convert message to bits
    obj.pPayloadBits = obj.PayloadMessage;

    % Calculate number of OFDM symbols per frame
    obj.pNumOFDMSymbols = ceil(length(obj.pPayloadBits)/obj.NumDataCarriers/2);
    % Calculate number of bits padded in each frame
    obj.pNumPadBits = 2*obj.NumDataCarriers * obj.pNumOFDMSymbols - length(obj.pPayloadBits);
    
    % Get preamble for each frame
    obj.pPreamble = coder.const(double(getOFDMPreambleAndPilot('Preamble', obj.FFTLength, obj.NumGuardBandCarriers)));

    % Get pilot for each frame
    obj.pPilots = coder.const(double(getOFDMPreambleAndPilot('Pilot', obj.pNumOFDMSymbols)));

    % QPSK modulator
    obj.pQPSKMod = comm.QPSKModulator('BitInput',true);

    % OFDM modulator
    obj.pDataOFDMMod = comm.OFDMModulator(...
        'FFTLength' ,           obj.FFTLength, ...
        'NumGuardBandCarriers', obj.NumGuardBandCarriers, ...
        'InsertDCNull',         true, ...
        'PilotInputPort',       true, ...
        'PilotCarrierIndices',  obj.PilotCarrierIndices, ...
        'CyclicPrefixLength',   obj.CyclicPrefixLength, ...
        'NumSymbols',           obj.pNumOFDMSymbols);
  end
  
  function resetImpl(obj)
    reset(obj.pQPSKMod);
    reset(obj.pDataOFDMMod);
  end
  
  function y = stepImpl(obj)    
    % QPSK modulation for one frame
    symPostQPSK = obj.pQPSKMod(...
        [obj.pPayloadBits; randi([0 1], obj.pNumPadBits, 1)]);
  
    % OFDM modulation for one frame
    symPostOFDM = obj.pDataOFDMMod(...
        reshape(symPostQPSK, obj.NumDataCarriers, obj.pNumOFDMSymbols), obj.pPilots);
    
    % Repeat the frame
    y = repmat([obj.pPreamble; symPostOFDM], obj.NumFrames, 1);
  end
  
  function releaseImpl(obj)
    release(obj.pQPSKMod);
    release(obj.pDataOFDMMod);
  end
  
  function s = saveObjectImpl(obj)
    s = saveObjectImpl@matlab.System(obj);
    if isLocked(obj)
        s.pQPSKMod        = matlab.System.saveObject(obj.pQPSKMod);
        s.pDataOFDMMod    = matlab.System.saveObject(obj.pDataOFDMMod);
        s.pPayloadBits    = obj.pPayloadBits;
        s.pNumPadBits     = obj.pNumPadBits;
        s.pNumOFDMSymbols = obj.pNumOFDMSymbols;
        s.pPreamble       = obj.pPreamble;
        s.pPilots         = obj.pPilots;
    end      
  end
  
  function loadObjectImpl(obj, s, wasLocked)
    if wasLocked
        obj.pQPSKMod        = matlab.System.loadObject(s.pQPSKMod);
        obj.pDataOFDMMod    = matlab.System.loadObject(s.pDataOFDMMod);
        obj.pPayloadBits    = s.pPayloadBits;
        obj.pNumPadBits     = s.pNumPadBits;
        obj.pNumOFDMSymbols = s.pNumOFDMSymbols;
        obj.pPreamble       = s.pPreamble;
        obj.pPilots         = s.pPilots;
    end
    loadObjectImpl@matlab.System(obj, s);
  end

  function num = getNumInputsImpl(~)
    num = 0;
  end
    
  function flag = isOutputComplexityLockedImpl(~,~)
    flag = true;
  end
end

end

% [EOF]
