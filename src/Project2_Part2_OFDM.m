clear all
close all

% Initialization
tx_bits=randi([0 1],1e4,1);
numFrames = 1e2;        

% Transmitter
[txSig, frameLen] = generateOFDMSignalQPSK(tx_bits, numFrames);

% Ideal Channel

sps = 1;
fs = 20e6*sps;

a_db = [-3.41 -0.93 -0.03 0.00 -0.59 -1.71 -3.30 -5.33 -7.77 -10.61];
t = (5e-8*(0:0.2:1.8));

n_channel = 1;

chan = comm.RayleighChannel(...
			     'SampleRate',fs, ...
			     'PathDelays',t, ...
			     'AveragePathGains',a_db, ...
			     'MaximumDopplerShift',0,...
			     'FadingTechnique', 'Sum of sinusoids');

rxSig = chan(txSig);

% Receiver
[rx_bits, numFramesDetected] = receiveOFDMSignalQPSK(rxSig, frameLen);

% Calculate average BER
BER = calculateOFDMBERQPSK(tx_bits, rx_bits, numFramesDetected)
