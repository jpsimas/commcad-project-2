function [BER] = calculateOFDMBERQPSK(msg, decMsgInBits, numFramesDetected)
% CALCULATEOFDMBER: BER calculation based on the repeatedly transmitted
% message and the detected frames.

% Reconstruct transmitted bit message 
txMsgInBits    = msg;
txMsgInBitsAll = repmat(txMsgInBits, 1, numFramesDetected);
bitMsgLen      = length(txMsgInBits);

% Remove padded bits from detected frames
rxMsgInBits    = reshape(decMsgInBits, length(decMsgInBits)/numFramesDetected, numFramesDetected);
rxMsgInBitsAll = rxMsgInBits(1:bitMsgLen, :); 

% BER calculation
numBitErr = sum(txMsgInBitsAll(:) ~= rxMsgInBitsAll(:));
BER = numBitErr/(bitMsgLen*numFramesDetected);