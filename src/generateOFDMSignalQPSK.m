function [txSig, frameLen] = generateOFDMSignalQPSK(msg, numFrames)
%GENERATEOFDMSIGNAL: Generate OFDM signal based on the 802.11a standard.
% This function returns the time domain signal and the frame length.

OFDMTX = OFDMTransmitterQPSK('PayloadMessage', msg, 'NumFrames', numFrames);
txSig = OFDMTX();
frameLen = OFDMTX.FrameLength;

% [EOF]