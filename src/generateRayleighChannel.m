%% -*- octave -*-
%% commcad-project-2
%% Copyright (C) 2020 João Pedro de O. Simas

%% This program is free software: you can redistribute it and/or
%% modify it under the terms of the GNU General Public License as
%% published by the Free Software Foundation, either version 3 of the
%% License, or (at your option) any later version.

%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.

%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see
%% <https://www.gnu.org/licenses/>.

function h = generateRayleighChannel(n_channel, a, t, fc)
  h = zeros(n_channel, 1);

  t = t + floor((n_channel - 1)/2);%%cemter the channel response

  ind = (0:(n_channel-1)).';
  
  for i = 1:length(a)
    h = h + a(i)*exp(-1j*2*pi/n_channel*ind*t(i))*...
    exp(-1j*2*pi*fc*t(i));
  end;
  
  h = ifft(h);

  h = h./norm(h);%%normalize expected filter
  
  h = (randn(size(h)) + 1j*randn(size(h)))/sqrt(2).*h;
  
  %%h = h(length(h):-1:1);

  %{
  n_channel = max(t);
  
  for i = 1:length(a)
    t(i)
    h(round(t(i)) + 1) = a(i)*randn()*exp(-1j*2*pi*fc*t(i));
  end;
%}
