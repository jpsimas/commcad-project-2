%% -*- octave -*-
%% commcad-project-2
%% Copyright (C) 2020 João Pedro de O. Simas

%% This program is free software: you can redistribute it and/or
%% modify it under the terms of the GNU General Public License as
%% published by the Free Software Foundation, either version 3 of the
%% License, or (at your option) any later version.

%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.

%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see
%% <https://www.gnu.org/licenses/>. 
clear all
close all
format long e

%%sps = 4;%upsampling factor
n_channel = 33;

%%rayleigh params
a_db = [-3.41 -0.93 -0.03 0.00 -0.59 -1.71 -3.30 -5.33 -7.77 -10.61];...
t = (5e-8*(0:0.2:1.8));
t = t.';
a = (10.^(a_db.'/20));

%%PT3
%%read optimum parameters and reference SNRs
a = csvread("ffe.txt");
SNR_ref_ffe = a(1); mu_ffe = a(2); frame_length_ffe = a(3);
a = csvread("dfe.txt");
SNR_ref_dfe = a(1); mu_dfe = a(2); frame_length_dfe = a(3);
SNR_ref_ofdm = csvread("ofdm.txt");


%%generate rayleigh channel
n_realiz = 100;

BER_ffe = zeros(n_realiz, 1);
BER_dfe = zeros(n_realiz, 1);
BER_ofdm = zeros(n_realiz, 1);

disp("BEGIN")

tic;
for i = 1:n_realiz

  if mod(i - 1, 5) == 0
    disp(strcat("running realiz: ", num2str(i - 1,'%d')));
  end;
  
  sps = 4;
  bit_rate = 18e6*(5/4);
  symb_rate = bit_rate/2;
  fs = symb_rate*sps;
  fc = 5e9/fs;
  
  h_chan = generateRayleighChannel(n_channel, a, fs*t, fc);

  %%FFE
  SNR = SNR_ref_ffe^(1.1);
  mu = mu_ffe;
  frame_length = frame_length_ffe;
  n_frames = 2;
  n_sim_realiz = 5;


  n_ff = 32;
  n_fb = 0;

  use_rayleigh = true;
  [BER_ffe(i), e] = qpsk_sim(n_frames, sps, bit_rate, SNR,...
			     use_rayleigh, n_ff, n_fb, mu,...
			     frame_length, h_chan, n_sim_realiz);
  
  %%DFE
  SNR_ref = SNR_ref_dfe^1.1;
  mu =  mu_dfe;
  frame_length = frame_length_dfe;

  n_ff = 32;
  n_fb = 5;

  use_rayleigh = true;
  [BER_dfe(i), e] = qpsk_sim(n_frames, sps, bit_rate, SNR,...
			     use_rayleigh, n_ff, n_fb, mu,...
			     frame_length, h_chan, n_sim_realiz);

  %%OFDM
  n_frames = 10;
  sps = 10;
  fs = 20e6;
  fs = fs*sps;
  fc = 5e9/fs;
  
  h_chan = generateRayleighChannel(n_channel, a, fs*t, fc);
  
  SNR = SNR_ref_ofdm^1.1;
  
  BER_ofdm(i) = ofdm_sim(n_frames, sps, SNR, h_chan);
  toc;
end;

csvwrite("BER_ffe.txt", BER_ffe)
csvwrite("BER_dfe.txt", BER_dfe)
csvwrite("BER_ofdm.txt", BER_ofdm)
