%% -*- octave -*-
%% commcad-project-2
%% Copyright (C) 2020 João Pedro de O. Simas

%% This program is free software: you can redistribute it and/or
%% modify it under the terms of the GNU General Public License as
%% published by the Free Software Foundation, either version 3 of the
%% License, or (at your option) any later version.

%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.

%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see
%% <https://www.gnu.org/licenses/>.
close all
clear all

BER_ffe = csvread("BER_ffe.txt");
BER_dfe = csvread("BER_dfe.txt");
BER_ofdm = csvread("BER_ofdm.txt");

n_pts = 1e5;
BER_pts = (round(n_pts*1e-5):round(n_pts*1e-1)).'/n_pts;

%{
figure;
hist(log10(BER_ffe));

figure;
hist(log10(BER_dfe));

figure;
hist(log10(BER_ofdm));
%}

%%remove invalid points
BER_ffe = BER_ffe(find(BER_ffe ~= 0));
BER_dfe = BER_dfe(find(BER_dfe ~= 0));
BER_ofdm = BER_ofdm(find(BER_ofdm ~= 2));

cdf_ffe = calc_cdf(BER_ffe, BER_pts);
cdf_dfe = calc_cdf(BER_dfe, BER_pts);
cdf_ofdm = calc_cdf(BER_ofdm, BER_pts);

figure;
hold on;
plot(log10(BER_pts), cdf_ffe)
plot(log10(BER_pts), cdf_dfe)
plot(log10(BER_pts), cdf_ofdm)
legend("FFE", "DFE", "OFDM")
title("Empirical CDFs of BER");
xlabel("log_{10} BER");
ylabel("F_{BER_x} (BER)");

print -dpng 3_cdf.png

p_outage_ffe = 1 - interp1(BER_pts + 1e-12*randn(size(BER_pts)),...
			   cdf_ffe, 1e-3, 'pchip')
p_outage_dfe = 1 - interp1(BER_pts + 1e-12*randn(size(BER_pts)),...
			   cdf_dfe, 1e-3, 'pchip')
p_outage_ofdm = 1 - interp1(BER_pts + 1e-12*randn(size(BER_pts)),...
			    cdf_ofdm, 1e-3, 'pchip')
