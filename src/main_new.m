%% -*- octave -*-
%% commcad-project-2
%% Copyright (C) 2020 João Pedro de O. Simas

%% This program is free software: you can redistribute it and/or
%% modify it under the terms of the GNU General Public License as
%% published by the Free Software Foundation, either version 3 of the
%% License, or (at your option) any later version.

%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.

%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see
%% <https://www.gnu.org/licenses/>.
format long e;
close all;
clear all;

currver = ver;
isOctave = strcmp(currver(1).Name, 'Octave');
if(isOctave)
  pkg load signal;
  pkg load communications;
end;

%%PT1 A

%%QPSK + FFE
SNR = 10.^((5:1:12)/10);
%%SNR = 10^(1.4);
mu = 1e-3;
frame_length = 10000;

sps = 4;
bit_rate = 18e6*(5/4);%%effective rate of 18mbps
dictionary_length = 4;%%QPSK

n_frames = 2;
n_sim_realiz = 10;
n_ff = 32;
n_fb = 0;

h_channel = [1];
use_rayleigh = false;
[BER, e] = qpsk_sim(n_frames, sps, bit_rate, SNR, use_rayleigh, n_ff,...
		    n_fb, mu, frame_length, h_channel, n_sim_realiz);

SNR_ref_ffe = interp1(BER + 1e-12*randn(size(BER)), SNR, 1e-3, 'pchip'...
		     )

figure;
grid on;
plot(20*log10(abs(e(:,end))));
title("MSE (dB) AWGN channel (SNR = 10dB)");

fig_1_a = figure;
grid on;
hold on;
plot(10*log10(SNR), log10(BER));
xlabel("SNR (dB)");
ylabel("log_{10} (BER)");
title("BER (AWGN channel)");

BER_ideal = 2*qfunc(sqrt(SNR));
plot(10*log10(SNR), log10(BER_ideal));

legend("AWGN", "QPSK ideal");

print -dpng 1_a_ffe_ber.png

%%Rayleigh vs awgn generate rayleigh channel
m_channel = 16;
n_channel = 2*m_channel + 1;

a_db = [-3.41 -0.93 -0.03 0.00 -0.59 -1.71 -3.30 -5.33 -7.77 -10.61];
t = (5e-8*(0:0.2:1.8));

%%rayleigh test
%%a_db = 0;
%%t = 0;

symb_rate = bit_rate/log2(dictionary_length);
fs = symb_rate*sps;
t = fs*t.';
a = (10.^(a_db.'/20));
fc = 5e9/fs;

n_realiz = 1;
BER_r = zeros(size(SNR));

for i = 1:n_realiz
  h_chan = generateRayleighChannel(n_channel, a, t, fc);

  figure;
  plot(abs(h_chan));
  title("h");

  use_rayleigh = true;
  [BER_ri, e] = qpsk_sim(n_frames, sps, bit_rate, SNR, use_rayleigh,...
			 n_ff, n_fb, mu, frame_length, h_chan,...
			 n_sim_realiz);
  BER_r = BER_r + BER_ri;
end;
BER_r = BER_r/n_realiz;

figure(fig_1_a);
grid on;
plot(10*log10(SNR), log10(BER_r));
legend("AWGN", "QPSK ideal", "Rayleigh");

print -dpng 1_a_ffe_ber_w_rayleigh.png

figure;
grid on;
plot(20*log10(abs(e(:,end))));
title("MSE (dB) Rayleigh channel (SNR = 10dB)");

print -dpng 1_a_ffe_mse_rayleigh.png

%%QPSK + DFE
n_ff = 32;
n_fb = 5;

h_chan = [1];
use_rayleigh = false;
[BER, e] = qpsk_sim(n_frames, sps, bit_rate, SNR, use_rayleigh, n_ff,...
		    n_fb, mu, frame_length, h_chan, n_sim_realiz);
BER

SNR_ref_dfe = interp1(BER + 1e-12*randn(size(BER)), SNR, 1e-3, 'pchip'...
		     )

figure;
grid on;
plot(20*log10(abs(e(:,end))));
title("MSE (dB) AWGN channel (SNR = 10dB)");

fig_1_b = figure;
grid on;
hold on;
plot(10*log10(SNR), log10(BER));
xlabel("SNR (dB)");
ylabel("10 log_{10} (BER)");
title("BER (AWGN channel)");

BER_ideal = 2*qfunc(sqrt(SNR));
plot(10*log10(SNR), log10(BER_ideal));
legend("AWGN", "QPSK ideal");

figure(fig_1_b)
print -dpng 1_a_dfe_ber.png

use_rayleigh = true;
for i = 1:n_realiz
  [BER_ri, e] = qpsk_sim(n_frames, sps, bit_rate, SNR, use_rayleigh,...
			 n_ff, n_fb, mu, frame_length, h_chan,...
			 n_sim_realiz);...
  BER_r = BER_r + BER_ri;
end;
BER_r = BER_r/n_realiz;

figure(fig_1_b);
grid on;
plot(10*log10(SNR), log10(BER_r));
legend("AWGN", "QPSK ideal", "Rayleigh");

print -dpng 1_a_dfe_ber_w_rayleigh.png

figure;
grid on;
plot(20*log10(abs(e(:,end))));
title("MSE (dB) Rayleigh channel (SNR = 10dB)");

print -dpng 1_a_dfe_mse_rayleigh.png

%%PLOT CONTOURS

[mu_mesh fl_mesh] = meshgrid(logspace(-5, -1, 6),...
			     round(logspace(2, 4, 4)))

BER_mesh = zeros(size(mu_mesh));

n_realiz = 10;
n_frames = 2;
n_sim_realiz = 3;

%%FFE
n_ff = 32;
n_fb = 0;

SNR = SNR_ref_ffe^1.1;
use_rayleigh = true;

for k = 1:n_realiz
  h_chan = generateRayleighChannel(n_channel, a, t, fc);

  for k1 = 1:(size(mu_mesh, 1))
    for k2 = 1:(size(mu_mesh, 2))
      BER_mesh(k1,k2) = ...
      BER_mesh(k1,k2) + qpsk_sim(n_frames, sps, bit_rate, SNR,...
				 use_rayleigh, n_ff, n_fb,...
				 mu_mesh(k1,k2), fl_mesh(k1,k2),...
				 h_chan, n_sim_realiz);...
    end;
  end;
end;

BER_mesh = BER_mesh/n_realiz;

[bermin, ibermin] = min(BER_mesh);
[bermin, ibermin2] = min(bermin);
ibermin = [ibermin(ibermin2); ibermin2]

mu_ffe_opt = mu_mesh(ibermin(1), ibermin(2));
frame_length_ffe_opt = fl_mesh(ibermin(1), ibermin(2));

fig_1_b_countour_ffe = figure;
figure(fig_1_b_countour_ffe);
contour(log10(mu_mesh),log10(fl_mesh),log10(BER_mesh),'ShowText','on')
title("log_{10}(BER)");
xlabel("log_{10}\mu");
ylabel("log_{10} n_{frame}");

print -dpng 1_b_countour_ffe.png;

%%DFE
n_ff = 32;
n_fb = 5;

SNR = SNR_ref_dfe^1.1;
use_rayleigh = true;

BER_mesh = zeros(size(mu_mesh));
for k = 1:n_realiz
  h_chan = generateRayleighChannel(n_channel, a, t, fc);
  
  for k1 = 1:(size(mu_mesh, 1))
    for k2 = 1:(size(mu_mesh, 2))
      BER_mesh(k1,k2) =...
      BER_mesh(k1,k2) + qpsk_sim(n_frames, sps, bit_rate, SNR,...
				 use_rayleigh, n_ff, n_fb,...
				 mu_mesh(k1,k2), fl_mesh(k1,k2),...
				 h_chan, n_sim_realiz);...
    end;
  end;
end;

BER_mesh = BER_mesh/n_realiz;

[bermin, ibermin] = min(BER_mesh);
[bermin, ibermin2] = min(bermin);
ibermin = [ibermin(ibermin2); ibermin2]

mu_dfe_opt = mu_mesh(ibermin(1), ibermin(2));
frame_length_dfe_opt = fl_mesh(ibermin(1), ibermin(2));

fig_1_b_countour_dfe = figure;
contour(log10(mu_mesh),log10(fl_mesh),log10(BER_mesh),'ShowText','on')
title("log_{10}(BER)");
xlabel("log_{10}\mu");
ylabel("log_{10} n_{frame}");


print -dpng 1_b_countour_dfe.png;

%%re-run initial simulation with optimized parameters

%%QPSK + FFE
SNR = 10.^((5:1:12)/10);
mu = mu_ffe_opt;
frame_length = frame_length_ffe_opt;

sps = 4;
bit_rate = 18e6*(6/5);%%effective rate of 18mbps
dictionary_length = 4;%%QPSK

n_frames = 2;
n_sim_realiz = 10;
n_ff = 32;
n_fb = 0;

h_channel = [1];
use_rayleigh = false;
[BER, e] = qpsk_sim(n_frames, sps, bit_rate, SNR, use_rayleigh, n_ff,...
		    n_fb, mu, frame_length, h_channel, n_sim_realiz);

SNR_ref_ffe = interp1(BER + 1e-12*randn(size(BER)), SNR, 1e-3, 'pchip')

figure;
grid on;
plot(20*log10(abs(e(:,end))));
title("MSE (dB) AWGN channel (SNR = 10dB)");

fig_1_a_opt = figure;
grid on;
hold on;
plot(10*log10(SNR), log10(BER));
xlabel("SNR (dB)");
ylabel("log_{10} (BER)");
title("BER (AWGN channel)");

BER_ideal = 2*qfunc(sqrt(SNR));
plot(10*log10(SNR), log10(BER_ideal));

legend("AWGN", "QPSK ideal");

print -dpng 1_a_opt_ffe_ber.png

%%Rayleigh vs awgn
%%generate rayleigh channel
m_channel = 16;
n_channel = 2*m_channel + 1;

a_db = [-3.41 -0.93 -0.03 0.00 -0.59 -1.71 -3.30 -5.33 -7.77 -10.61];
t = (5e-8*(0:0.2:1.8));

symb_rate = bit_rate/log2(dictionary_length);
fs = symb_rate*sps;
t = fs*t.';
a = (10.^(a_db.'/20));
fc = 5e9/fs;

n_realiz = 1;
BER_r = zeros(size(SNR));

for i = 1:n_realiz
  h_chan = generateRayleighChannel(n_channel, a, t, fc);

  figure;
  plot(abs(h_chan));
  title("h");

  use_rayleigh = true;
  [BER_ri, e] = qpsk_sim(n_frames, sps, bit_rate, SNR, use_rayleigh,...
			 n_ff, n_fb, mu, frame_length, h_chan,...
			 n_sim_realiz);
  BER_r = BER_r + BER_ri;
end;
BER_r = BER_r/n_realiz;

figure(fig_1_a_opt);
grid on;
plot(10*log10(SNR), log10(BER_r));
legend("AWGN", "QPSK ideal", "Rayleigh");

print -dpng 1_a_opt_ffe_ber_w_rayleigh.png

figure;
grid on;
plot(20*log10(abs(e(:,end))));
title("MSE (dB) Rayleigh channel (SNR = 10dB)");

print -dpng 1_a_ffe_mse_rayleigh.png

%%QPSK + DFE
mu = mu_dfe_opt;
frame_length = frame_length_dfe_opt;

n_ff = 32;
n_fb = 5;

h_chan = [1];
use_rayleigh = false;
[BER, e] = qpsk_sim(n_frames, sps, bit_rate, SNR, use_rayleigh, n_ff,...
		    n_fb, mu, frame_length, h_chan, n_sim_realiz);
BER

SNR_ref_dfe = interp1(BER + 1e-12*randn(size(BER)), SNR, 1e-3, 'pchip')

csvwrite("ffe.txt", [SNR_ref_ffe mu_ffe_opt frame_length_ffe_opt])
csvwrite("dfe.txt", [SNR_ref_dfe mu_dfe_opt frame_length_dfe_opt])

figure;
grid on;
plot(20*log10(abs(e(:,end))));
title("MSE (dB) AWGN channel (SNR = 10dB)");

fig_1_b_opt = figure;
grid on;
hold on;
plot(10*log10(SNR), log10(BER));
xlabel("SNR (dB)");
ylabel("10 log_{10} (BER)");
title("BER (AWGN channel)");

BER_ideal = 2*qfunc(sqrt(SNR));
plot(10*log10(SNR), log10(BER_ideal));
legend("AWGN", "QPSK ideal");

figure(fig_1_b_opt)
print -dpng 1_a_opt_dfe_ber.png
