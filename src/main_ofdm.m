%% -*- octave -*-
%% commcad-project-2
%% Copyright (C) 2020 João Pedro de O. Simas

%% This program is free software: you can redistribute it and/or
%% modify it under the terms of the GNU General Public License as
%% published by the Free Software Foundation, either version 3 of the
%% License, or (at your option) any later version.

%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.

%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see
%% <https://www.gnu.org/licenses/>.

clear all 
close all

n_frames = 10;
% channel
fs = 20e6;
sps = 10;

fs = fs*sps;

m_channel = 4*sps;
n_channel = 2*m_channel + 1;

a_db = [-3.41 -0.93 -0.03 0.00 -0.59 -1.71 -3.30 -5.33 -7.77 -10.61];
t = (5e-8*(0:0.2:1.8));

%%test
%%a_db = 0;
%%t = 0;

t = fs*t.';
a = (10.^(a_db.'/20));
fc = 5e9/fs;

h_chan_rayleigh = generateRayleighChannel(n_channel, a, t, fc);

%%AWGN channel
disp("AWGN")
h_chan = zeros(n_channel, 1);
h_chan(m_channel) = 1;

SNR = 10.^((5:1:12)/10);

BER = ofdm_sim(n_frames, sps, SNR, h_chan);

fig1 = figure;
hold on;
plot(10*log10(SNR), log10(BER));
xlabel("SNR (dB)");
ylabel("log_{10} (BER)");
title("BER (AWGN channel)");

BER_ideal = 2*qfunc(sqrt(SNR));
plot(10*log10(SNR), log10(BER_ideal));
legend("AWGN", "Ideal QPSK");

print -dpng 2_ofdm_ber.png

SNR_ref = interp1(BER + 1e-12*randn(size(BER)), SNR, 1e-3, 'pchip')
csvwrite("ofdm.txt", SNR_ref);

%%Rayleigh channel
disp("RAYLEIGH")
h_chan = h_chan_rayleigh;

figure;
plot(abs(h_chan));
title("h");

BER = ofdm_sim(n_frames, sps, SNR, h_chan);

figure(fig1);
plot(10*log10(SNR), log10(BER));
xlabel("SNR (dB)");
ylabel("10 log_{10} (BER)");

legend("AWGN", "Ideal QPSK", "Rayleigh");

print -dpng 2_ofdm_ber_w_rayleigh.png

hnorm = norm(h_chan_rayleigh)
