%% -*- octave -*-
%% commcad-project-2
%% Copyright (C) 2020 João Pedro de O. Simas

%% This program is free software: you can redistribute it and/or
%% modify it under the terms of the GNU General Public License as
%% published by the Free Software Foundation, either version 3 of the
%% License, or (at your option) any later version.

%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.

%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see
%% <https://www.gnu.org/licenses/>. 

function BER = ofdm_sim(n_frames, sps, SNR, h_chan)

  b = randi([0 1],1e4,1);

  [x, frame_len] = generateOFDMSignalQPSK(b, n_frames);

  %%append some zeros, just because
  %%x = [x; zeros(length(h_chan), 1)];

  use_matlab_thing = false;
  if(use_matlab_thing && length(h_chan) ~= 1)
    fs = 20e6*sps;
    
    a_db = [-3.41 -0.93 -0.03 0.00 -0.59 -1.71 -3.30 -5.33 -7.77 ...
	    -10.61];
    t = (5e-8*(0:0.2:1.8));

    n_channel = 1;
    
    chan = comm.RayleighChannel(...
				 'SampleRate',fs, ...
				 'PathDelays',t, ...
				 'AveragePathGains',a_db, ...
				 'MaximumDopplerShift',0,...
				 'FadingTechnique', 'Sum of sinusoids');
  end;
  
  BER = zeros(size(SNR));

			 % upsampling
  %%x_us = zeros(sps*length(x) + (sps - 1), 1);
  %%x_us(sps:sps:end) = x;
  x_us = resample(x, sps, 1);
  
  x_us = x_us/(sqrt(sps)*std(x_us));

  x_us = [x_us; zeros((length(h_chan) - 1)/2, 1)];
  n = (randn(size(x_us)) + 1j*randn(size(x_us)))/sqrt(2);

  %%x_us = [x_us; zeros(frame_len, 1)];
  
  for k = 1:length(SNR)
    bw_normalized = 16.6e6/20e6;
    sigma_n = sqrt(1./(SNR(k)*bw_normalized));
    %%x_us_w_noise = x_us + (randn(size(x_us)) +...
    %%1j*randn(size(x_us)))/sqrt(2)*sigma_n;
    
    x_us_n = x_us + sigma_n*n;

    if(~use_matlab_thing || length(h_chan) == 1)
      %%x_ch_us = filter_fir(h_chan, x_us);
      
      %%x_us = [x_us; zeros(frame_len, 1)];
      x_ch_us = filter(h_chan, [1], x_us_n);
      
      %%x_ch_us = x_ch_us(((length(h_chan) + 1)/2):end);%%manual...
      %%synchronization because function does not work x_ch_us =
      %%x_ch_us(((length(h_chan) + 1)/2):end);
    else
      x_ch_us = chan(x_us);
    end;

    
    x_ch_us = x_ch_us/(sqrt(sps)*std(x_ch_us));
    %%x_ch_us = x_ch_us + (randn(size(x_ch_us)) +...
    %%1j*randn(size(x_ch_us)))/sqrt(2)*sigma_n; x_ch_us = x_ch_us +
    %%sigma_n*n;
    
    %%downsampling
    %%x_ch = x_ch_us(sps:sps:end);%%this is broken
    x_ch = resample(x_ch_us, 1, sps);
    
    %% Receiver
    [rx_bits, numFramesDetected] =...
    receiveOFDMSignalQPSK(x_ch, frame_len);
    numFramesDetected
    size(rx_bits)

    %% Calculate average BER
    if(numFramesDetected == 0)
      BER(k) = 2;
    else
      BER(k) = calculateOFDMBERQPSK(b, rx_bits, numFramesDetected);
    end;

    frame_len
  end;
