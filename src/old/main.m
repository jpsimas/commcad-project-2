%% -*- octave -*-
%% commcad-project-2
%% Copyright (C) 2020 João Pedro de O. Simas

%% This program is free software: you can redistribute it and/or
%% modify it under the terms of the GNU General Public License as
%% published by the Free Software Foundation, either version 3 of the
%% License, or (at your option) any later version.

%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.

%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see
%% <https://www.gnu.org/licenses/>. 

close all;
clear all;

%QPSK symbols
dictionary = [1+j, -1+j, -1-j, 1-j]/sqrt(2);

%training sequence
tr_seq = dictionary([1 2 4 3 3 4 2 1]);

%generate symbols
n_sym = 100000;
frame_length = 10000;
training_syms = frame_length/5;

x = dictionary(randi(length(dictionary), n_sym, 1)).';

%up-sampling
sps = 16;%upsampling factor
x_us = zeros(sps*length(x) + (sps - 1), 1);
x_us(sps:sps:end) = x;

%RRC pulse
rho = 0.2;
M = 10;
f = (-sps/2 : 1/(2*(M+1)) : sps/2-1/(2*(M+1)));

i1 = find(abs(f) <= (1-rho)/2);
i2 = find((abs(f) <= (1+rho)/2) & ((1-rho)/2 <= abs(f)));

H = zeros(size(f));
H(1, i1) = sps;
H(1, i2) = (1 - sin(pi/rho*(abs(f(i2))-1/2)))*sps/2;
H = sqrt(fftshift(H));

b_rrc = real(fftshift(ifft(H)));

%pulse_shaping
x_ps = filter_fir(b_rrc, x_us);
%channel
bit_rate = 18e6;
symb_rate = bit_rate/log2(length(dictionary));

n_channel = 32;
a = (10.^([-3.41 -0.93 -0.03 0.00 -0.59 -1.71 -3.30 -5.33 -7.77 -10.61]/20)).';
a = sqrt(2/pi)*sqrt(randn(size(a)).^2 + randn(size(a)).^2).*a;%generate channel gains
t = (5e-8*(0:0.2:1.8)).';

f = symb_rate*sps*(0:(n_channel - 1)).'/n_channel;
h = zeros(n_channel, 1);
for i = 1:length(a)
  h = h + a(i)*exp(-1j*2*pi*(f + 5e9)*t(i) );
end;

h = ifft(h);

SNR = 1e3;
sigma_n = sqrt(1/SNR);%mean signal power is 1
x_channel = filter_fir(h, x_ps + sigma_n*(randn(size(x_ps)) + 1j*randn(size(x_ps)))/sqrt(2));

%downsampling
u = x_channel((sps/2):(sps/2):end);

%equalizer
n_u = 32;%ffe
n_s = 5;%dfe

%w_u(end) = 1;

mu = 1e-3;

% DFE
w = zeros(n_u + n_s, 1);
delta = 6 + length(b_rrc)/2/16 - 1 + n_u/8;
y = zeros(floor((length(u) - 2*delta)/2), 1);
y_dec = zeros(floor((length(u) - 2*delta)/2), 1);
e = zeros(floor((length(u) - 2*delta)/2), 1);

for i = 1:floor((length(u) - 2*delta)/2)
  if(mod(i - 1, frame_length) < training_syms)
    ui = [u((1:n_u) + 2*i); x(i + delta - (1:n_s))];
  else
    ui = [u((1:n_u) + 2*i); y_dec(i - (1:n_s))];
  end;
  
  y(i) = dot(w, ui);
  y_dec(i) = (sign(real(y(i))) + 1j*sign(imag(y(i))))/sqrt(2);

  if(mod(i - 1, frame_length) < training_syms)
    d = x(i + delta);
  else
    d = y_dec(i);
  end;
  
  e(i) = d - y(i);
  w = w + mu*conj(e(i))*ui;
end;

scatterplot(y(4000:end));
figure;
plot(20*log10(abs(e)));
xlabel("Sample");
ylabel("MSE (dB)");

figure;
plot(abs(w));

figure;
e_new = y_dec - x((1:length(y)) + delta);
plot(abs(e_new));
ber = log2(length(dictionary)) * sum([e_new(4000:end) ~= 0])/length(e_new(4000:end))

