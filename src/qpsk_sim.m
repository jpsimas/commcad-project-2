%% -*- octave -*-
%% commcad-project-2
%% Copyright (C) 2020 João Pedro de O. Simas

%% This program is free software: you can redistribute it and/or
%% modify it under the terms of the GNU General Public License as
%% published by the Free Software Foundation, either version 3 of the
%% License, or (at your option) any later version.

%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.

%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see
%% <https://www.gnu.org/licenses/>. 

function [ber, e] = qpsk_sim(n_frames, sps, bit_rate, SNR,...
			     use_rayleigh, n_u, n_s, mu, frame_length,...
			     h_channel, n_realiz)
  
  %%Modulation Parameters
  %%QPSK symbols
  dictionary = [1+j, -1+j, -1-j, 1-j]/sqrt(2);
  symb_rate = bit_rate/log2(length(dictionary));

  %%normalize frequencies
  fs = symb_rate*sps;
  bit_rate = bit_rate/fs;
  symb_rate = symb_rate/fs;
  
  %%generate symbols
  n_sym = frame_length*n_frames;
  training_syms = floor(frame_length/5);

  x = dictionary(randi(length(dictionary), n_sym, 1)).';

%upsampling
  x_us = zeros(sps*length(x) + (sps - 1), 1);
  x_us(sps:sps:end) = x;

  
				%RRC pulse
  rho = 0.2;
  M = 10;
  f = (-sps/2 : 1/(2*(M+1)) : sps/2-1/(2*(M+1)));

  i1 = find(abs(f) <= (1-rho)/2);
  i2 = find((abs(f) <= (1+rho)/2) & ((1-rho)/2 <= abs(f)));
  
  H = zeros(size(f));
  H(1, i1) = sps;
  H(1, i2) = (1 - sin(pi/rho*(abs(f(i2))-1/2)))*sps/2;
  H = sqrt(fftshift(H));

  b_rrc = real(fftshift(ifft(H)));

  clear H;

  %%pulse_shaping
  x_ps = filter_fir(b_rrc, x_us);
  x_ps = x_ps/std(x_ps);%%normalize power
  
  clear x_us;
  
  %%channel
  sigma_n = sqrt(sps./(SNR));

  ber = zeros(1, length(SNR));
  for m = 1:n_realiz

    use_matlab_thing = false;

    if(~use_matlab_thing || ~use_rayleigh)

      n_channel = length(h_channel);

      x_ps_n = x_ps + (randn(size(x_ps)) + 1j*randn(size(x_ps)))/...
      sqrt(2)*sigma_n;
      
      x_channel = filter_fir(h_channel, x_ps_n);
      
    else
      a_db = [-3.41 -0.93 -0.03 0.00 -0.59 -1.71 -3.30 -5.33 -7.77 -...
	      10.61];
      t = (5e-8*(0:0.2:1.8));

      n_channel = 1;
      
      chan = comm.RayleighChannel(...
				   'SampleRate',fs, ...
				   'PathDelays',t, ...
				   'AveragePathGains',a_db, ...
				   'MaximumDopplerShift',0);

      x_channel = chan(x_ps);

    end;

    %%downsample to sps = 2;
    x_ds = resample(x_channel, 2, sps);

    clear x_channel

    %%normalize power before equalization
    %%x_ds = x_ds./std(x_ds);
    
    %%equalizer
    
    %% DFE
    n_w = n_u + n_s;

    ber_calc_begin = frame_length;

    delta = round(((n_channel - 1)/2 + (length(b_rrc) - 1)/2)/sps...
		  + ((n_u - 1)/2)/2);

    %%define equalizer vectors
    y = zeros(floor((length(x_ds) - 2*delta - n_u)/2), length(SNR));
    
    y_dec = zeros(floor((length(x_ds) - 2*delta - n_u)/2), length(SNR));
    e = zeros(floor((length(x_ds) - 2*delta - n_u)/2), length(SNR));...

    u = x_ds;

    clear x_ds;
    
    %%DFE LMS EQUALIZER
    w = zeros(n_w, length(SNR));

    %%initialize vectors
    y(:) = 0;
    y_dec(:) = 0;
    e(:) = 0;
    
    for i = 1:floor((length(u) - 2*delta - n_u)/2)
      if(mod(i - 1, frame_length) < training_syms)
	for k = 1:length(SNR)
	  ui(:,k) = [u((1:n_u) + 2*i, k); x(i + delta - (1:n_s))];
	end;
      else
	ui = [u((1:n_u) + 2*i, :); y_dec(i - (1:n_s), :)];
      end;
      
      y(i, :) = dot(w, ui);
      y_dec(i, :) = (sign(real(y(i, :))) + 1j*sign(imag(y(i, :))))/...
      sqrt(2);

      if(mod(i - 1, frame_length) < training_syms)
	d = x(i + delta);
      else
	d = y_dec(i, :);
      end;

      ei = d - y(i, :);
      
      e(i, :) = e(i, :) + ei;
      w = w + mu*ui.*conj(ei);
    end;

    e_new = y_dec - x((1:length(y)) + delta);

    inds = (1:length(e_new)).';
    %%ignore preambles
    q = (mod(inds - 1, frame_length) >= training_syms);
    %%ignore first frame
    q = q.*((inds - 1) >= ber_calc_begin);

    %%ignore last symbols 
    q(end) = 0;
    
    e_new = e_new.*q;

    beri = sum(e_new ~= 0)./sum(q);

    ber = ber + beri;
    
  end;
  ber = ber/n_realiz;
end
