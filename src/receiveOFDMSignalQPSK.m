function [decMsgInBits, numFramesDetected] = receiveOFDMSignalQPSK(rxSig, frameLen)
% RECEIVEOFDMSIGNAL: Recover OFDM symbols from the time domain input signal
% and return the number of detected frames and the decoded bits from these
% detected frames.

ofdmrx = OFDMReceiverQPSK('FrameLength',    frameLen, ...
    'DisplayMessage', false, ...
    'ShowScopes',     false);

[decMsgInBits, numFramesDetected] = ofdmrx(rxSig);